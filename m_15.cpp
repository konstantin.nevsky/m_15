// m_15.cpp
//

#include <iostream>

void FindOddNumbers(int Limit, bool IsOdd)
{
    for (int i = int(IsOdd); i <= Limit; i += 2) {
        std::cout << i << "\n";
    }
}

int main()
{
    const int N = 20;
    FindOddNumbers(N, true);
}

